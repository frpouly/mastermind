#ifndef master_h
#define master_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* DECLARATION DES CONSTANTES SYMBOLIQUES */

#define NB_COLONNES 4
#define NB_LIGNES 10
#define NB_COULEURS 6

/* DECLARATION DES TYPES PERSONNELS 
 * et de leur typedef si besoin */

int GERME;

typedef struct {
	int bienp;
	int malp;
} combinaison;

/* DECLARATIONS DES METHODES */

void initialiser_solution(int tab[NB_COLONNES]);
void initialiser_plateau(int plateau[NB_LIGNES][NB_COLONNES + 2]);
combinaison compiler_proposition(int proposition[NB_COLONNES], int solution[NB_COLONNES]);
int mettre_a_jour_plateau(int proposition[NB_COLONNES], int solution[NB_COLONNES], int ligne_plateau[NB_COLONNES + 2]);
void afficher_plateau(FILE *file, int plateau[NB_LIGNES][NB_COLONNES + 2]);
void demander_proposition(int proposition[NB_COLONNES]);

#endif
