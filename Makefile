CC=gcc
CFLAGS=-W -Wall
LDFLAGS=
EXEC=mastermind

all: 			$(EXEC)

mastermind: 	master_texte.o master.o
				$(CC) -o $@ $^ $(LDFLAGS)

test: 			tests.o teZZt.o master.o
				$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
				$(CC) -o $@ -c $< $(CFLAGS)

clean:
			rm -rf *.o

mrproper:	clean
			rm -rf $(EXEC)