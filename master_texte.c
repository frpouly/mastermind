#include "master.h"
#include <stdio.h>

int main(int argc, char ** argv)
{
	int nb_propositions = 0;
	int plateau[NB_LIGNES][NB_COLONNES + 2];
	int solution[NB_COLONNES], proposition[NB_COLONNES];
	if(argc > 1)
		sscanf(argv[1], "%d", &GERME);
	initialiser_solution(solution);
	printf("Le germe actuel est : %d \n", GERME);
	initialiser_plateau(plateau);
	do {
		afficher_plateau(stdout, plateau);
		demander_proposition(proposition);
		nb_propositions++;
	} while(!mettre_a_jour_plateau(proposition, solution, plateau[nb_propositions - 1]) && nb_propositions<NB_LIGNES);
	afficher_plateau(stdout, plateau);
	if(plateau[nb_propositions - 1][NB_COLONNES] == NB_COLONNES)
		printf("Bravo vous avez gagné !!!\n");
	else
		printf("Vous avez perdu !\n");
	return 0;
}
