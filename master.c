#include "master.h"



/* Crée des nombres aléatoires pour chaque case du tableau */
void initialiser_solution(int tab[NB_COLONNES]) 
{
   int i;
   if(GERME == 0)
   		GERME = time(NULL);
   	srand(GERME);
   for(i = 0; i<NB_COLONNES; i++)
   		tab[i] = rand()%6 + 1;
}

/* Initialise toutes les cases d'un tableau à 2 dimensions à 0 */
void initialiser_plateau(int plateau[NB_LIGNES][NB_COLONNES + 2])
{
   int i,j, nb_col = NB_COLONNES + 2;
   for(i = 0; i<NB_LIGNES; i++)
   		for(j = 0; j<nb_col; j++)
   			plateau[i][j] = 0;
}

void construireTabRegarder(int tab[NB_COLONNES], int proposition[NB_COLONNES], int solution[NB_COLONNES])
{
	int i;
	for(i = 0; i < NB_COLONNES; i++)
		tab[i] = proposition[i] != solution[i];
}

int estMalPlace(int tabRegarder[NB_COLONNES], int solution[NB_COLONNES], int proposition)
{
	int i = 0, ret = 0;
	while((!tabRegarder[i] || solution[i] != proposition) && i < NB_COLONNES)
	{
		i++;
	}
	if(tabRegarder[i] && solution[i] == proposition)
	{
		tabRegarder[i] = 0;
		ret = 1;
	}
	return ret;
}

combinaison compiler_proposition(int proposition[NB_COLONNES], int solution[NB_COLONNES]) 
{
	combinaison c;
	int i, tabRegarder[NB_COLONNES];
	c.malp = c.bienp = 0;
	construireTabRegarder(tabRegarder, proposition, solution);
	for(i = 0; i < NB_COLONNES; i++)
	{
		if(proposition[i] == solution[i])
			c.bienp++;
		else if(estMalPlace(tabRegarder, solution, proposition[i]))
			c.malp++;
	}
	return c;
}

void afficher_plateau(FILE *file, int plateau[NB_LIGNES][NB_COLONNES + 2])
{
	int i, j, nb_egals_haut = NB_COLONNES*3;
	int nb_egals_bas = (NB_COLONNES + 2) * 3;
	for(i = 0; i <= nb_egals_haut; i++)
		fprintf(file, "=");
	fprintf(file, "b=m==\n");
	for(i = 0; i < NB_LIGNES; i++)
	{
		fprintf(file, "| ");
		for(j = 0; j < NB_COLONNES; j++)
			fprintf(file, "%d ", plateau[i][j]);
		fprintf(file, "|| %d %d |\n", plateau[i][j], plateau[i][j+1]);
	}
	for(i = 0; i < nb_egals_bas; i++)
		fprintf(file, "=");
	fprintf(file, "\n");
}

/* La fonction permet de mettre à jour entièrement le plateau en fonction d'une 
 * proposition.
 * Elle renvoit un nombre positif si les réponses sont identiques, 0 sinon
*/
int mettre_a_jour_plateau(int proposition[NB_COLONNES], int solution[NB_COLONNES], int ligne_plateau[NB_COLONNES + 2])
{
	int i;
	combinaison c;
	for(i = 0; i < NB_COLONNES; i++)
		ligne_plateau[i] = proposition[i];
	c = compiler_proposition(proposition, solution);
	ligne_plateau[NB_COLONNES] = c.bienp;
	ligne_plateau[NB_COLONNES + 1] = c.malp;
	return c.bienp == NB_COLONNES;
}

void demander_proposition(int proposition[NB_COLONNES])
{
	char ligne[50];
	int i, valide = 1;
	do {
		printf("Saisir un jeu de couleur comme 1 1 2 3, valider par entrée : ");
		fgets(ligne, 10, stdin);
		i = 0;
		while(i < NB_COLONNES && sscanf(ligne, "%d", &proposition[i]) && proposition[i]>0 && proposition[i]<=NB_COULEURS)
		{
			strcpy(ligne, ligne + 2);
			i++;
		}
		valide = i == NB_COLONNES;
	} while(!valide);
}
